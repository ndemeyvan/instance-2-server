const express = require('express')
const app = express()
const port = 1000

app.get('/', (req, res) => {
  res.send('Hello World welcome to instance 2 !')
})

app.get('/i2', (req, res) => {
    res.send('Hello World welcome to instance 2 !')
})

app.listen(port, () => {
    console.log('Hello World welcome to instance 2!')
  console.log(`Example app listening at http://localhost:${port}`)
})